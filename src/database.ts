import { connect } from 'mongoose'

export default connect(process.env.MONGO_URI || 'mongodb://localhost:27017/apitsc', {
    useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false
}).then(() => {
    console.log('Base de datos conectada.');
}).catch(e => {
    console.log(e);
})