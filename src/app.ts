import express from 'express'
import morgan from 'morgan'
import cors from 'cors'
//inicializacion
const app = express();

//configuracion
app.set('port', process.env.PORT || 4000);

//middleware 
app.use(cors());
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

//rutas
app.get('/', (req, ress) => {
    ress.send(`Api Rest: http://localhost:${app.get('port')}`);
});

export default app;